package assignment;

import assignment.entity.Student;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {
    public List<Student> readCSV() {
        String file = "D:\\Student.csv";
        BufferedReader bufferedReader = null;
        String line = "";
        List<Student> students = new ArrayList<>();

        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            int i = 0;
            while ((line = bufferedReader.readLine()) != null) {
                i++;
                if (i == 1)
                    continue;
                String[] row = line.split(",");
                Student student = new Student();
                student.setName(row[0]);
                student.setAddress(row[1]);
                student.setRoll_number(Integer.parseInt(row[2]));
                student.setClassLevel(Integer.parseInt(row[3]));
                students.add(student);

            }
        } catch (IOException ex) {
            System.out.println("EX:" + ex.getStackTrace());

        }
        return students;
    }
}
