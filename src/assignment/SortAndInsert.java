package assignment;

import assignment.entity.Student;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class SortAndInsert {
    public void sortClassWise(List<Student> students) {
        ConfigureJDBC configureJDBC = new ConfigureJDBC();
        configureJDBC.createTable();
        //Grouping by class
        Map<Integer, List<Student>> studentsByClass = students.stream().collect(groupingBy(Student::getClassLevel));
        //Sorting by class and roll_number
        studentsByClass.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEachOrdered(std -> {
                    List<Student> sortedStudents = std.getValue().stream()
                            .sorted(Comparator.comparingInt(Student::getRoll_number))
                            .collect(Collectors.toList());
                    configureJDBC.insertQuery(sortedStudents);
                });
        configureJDBC.closeConnection();
    }
}
