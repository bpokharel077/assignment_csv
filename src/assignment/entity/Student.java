package assignment.entity;

public class Student {
    private String name;
    private String address;
    private Integer roll_number;
    private Integer classLevel;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getRoll_number() {
        return roll_number;
    }

    public void setRoll_number(Integer roll_number) {
        this.roll_number = roll_number;
    }

    public Integer getClassLevel() {
        return classLevel;
    }

    public void setClassLevel(Integer classLevel) {
        this.classLevel = classLevel;
    }
}
