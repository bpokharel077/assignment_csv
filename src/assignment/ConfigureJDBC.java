package assignment;

import assignment.entity.Student;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ConfigureJDBC {
    Connection con;
    String url;
    String userName;
    String password;

    public ConfigureJDBC() {
        this.userName = "root";
        this.password = "password";
        this.url = url = "jdbc:mysql://localhost:3306/test_database";
        try {
            this.con = jdbcConnection();
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("ex:" + ex.getStackTrace());
        }

    }

    private Connection jdbcConnection() throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection(url, userName, password);
        return con;
    }

    public void createTable() {
        try {
            Statement statement = con.createStatement();
            String createTableQuery = "create table students(" +
                    "id int(30) primary key auto_increment," +
                    "name varchar(200) not null," +
                    "address varchar(200) not null," +
                    "class int(30) not null," +
                    "roll_number int(30)" +
                    ")";
            statement.executeUpdate(createTableQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("student table created");
    }

    public void insertQuery(List<Student> students) {
        try {
            Statement statement = con.createStatement();
            students.forEach(student -> {
                String insertQuery = "Insert into students (name , address,class, roll_number)" +
                        " values(\'" + student.getName() + "\', \'" + student.getAddress() + "\', \'" + student.getClassLevel() + "\', \'" + student.getRoll_number() + "\')";
                try {
                    statement.executeUpdate(insertQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException ex) {
            System.out.println("ex:" + ex.getStackTrace());

        }
    }

    public void closeConnection() {
        try {
            con.close();
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
    }
}
