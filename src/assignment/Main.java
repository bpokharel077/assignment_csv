package assignment;

import assignment.entity.Student;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        ReadFile readFile = new ReadFile();
        List<Student> students= readFile.readCSV();
        SortAndInsert sortAndInsert = new SortAndInsert();
        sortAndInsert.sortClassWise(students);

    }
}
